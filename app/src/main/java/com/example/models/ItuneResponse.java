package com.example.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItuneResponse {
    @SerializedName("resultCount")
    @Expose
    private String resultCount;
    @SerializedName("results")
    @Expose
    private List<ItuneItem> results;

    /**
     * get result count
     * @return result count
     */

    public String getResultCount() {
        return resultCount;
    }

    /**
     * set result count
     * @param resultCount
     */

    public void setResultCount(String resultCount) {
        this.resultCount = resultCount;
    }

    /**
     * get results
     * @return results
     */

    public List<ItuneItem> getResults() {
        return results;
    }

    /**
     * set results
     * @param results
     */

    public void setResults(List<ItuneItem> results) {
        this.results = results;
    }
}
