package com.example.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author roljhonteano
 */

public class ItuneItem {

    @SerializedName("trackName")
    @Expose
    private String trackName;
    @SerializedName("artworkUrl100")
    @Expose
    private String artworkUrl100;
    @SerializedName("trackPrice")
    @Expose
    private Double trackPrice;
    @SerializedName("primaryGenreName")
    @Expose
    private String primaryGenreName;

    @SerializedName("longDescription")
    @Expose
    private String longDescription;

    /**
     * returns the value of the track name
     * @return The track name of type string
     */

    public String getTrackName() {
        return trackName;
    }

    /**
     * set the value of track name
     * @param trackName the value to be set as the track name
     */

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * returns the image url of the track name's artwork
     * @return The image url of the artwork
     */

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    /**
     * set artworkUrl100
     * @param artworkUrl100
     */

    public void setArtworkUrl100(String artworkUrl100) {
        this.artworkUrl100 = artworkUrl100;
    }

    /**
     * get trackPrice
     * @return trackPrice
     */

    public Double getTrackPrice() {
        return trackPrice;
    }

    /**
     * set trackPrice
     * @param trackPrice
     */

    public void setTrackPrice(Double trackPrice) {
        this.trackPrice = trackPrice;
    }

    /**
     * get primaryGenreName
     * @return primaryGenreName
     */

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    /**
     * set primaryGenreName
     * @param primaryGenreName
     */

    public void setPrimaryGenreName(String primaryGenreName) {
        this.primaryGenreName = primaryGenreName;
    }

    /**
     * get longDescription
     * @return longDescription
     */

    public String getLongDescription() {
        return longDescription;
    }

    /**
     * set longDescription
     * @param longDescription
     */
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
