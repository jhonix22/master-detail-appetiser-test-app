package com.example.persistence;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    private static PreferenceManager instance;

    private final String KEY_PREFERENCES = "com.example.masterdetailappetiser.preference";

    private Context context;


    // Last Login KEY
    private final String LAST_VISIT = "LAST_VISIT";

    /**
     * Constructor
     *
     * @param context
     * @return
     */
    public static PreferenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceManager();
            instance.context = context;
        }
        return instance;
    }

    // ======================== GETTER & SETTER ==================================

    public String getLAST_VISIT() {
        return getStringValue(LAST_VISIT);
    }

    public void setLAST_VISIT(String last_visit) {
        putStringValue(LAST_VISIT, last_visit);
    }


    // ======================== UTILITY METHODS ========================

    /**
     * Save a long integer to SharedPreferences
     *
     * @param key
     * @param n
     */
    public void putLongValue(String key, long n) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, n);
        editor.commit();
    }

    /**
     * Read a long integer to SharedPreferences
     *
     * @param key
     * @return
     */
    public long getLongValue(String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getLong(key, 0);
    }

    /**
     * Save an integer to SharedPreferences
     *
     * @param key
     * @param n
     */
    public void putIntValue(String key, int n) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, n);
        editor.commit();
    }

    /**
     * Read an integer to SharedPreferences
     *
     * @param key
     * @return
     */
    public int getIntValue(String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getInt(key, 0);
    }

    /**
     * Save a string to SharedPreferences
     *
     * @param key
     * @param s
     */
    public void putStringValue(String key, String s) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, s);
        editor.commit();
    }

    /**
     * Read a string to SharedPreferences
     *
     * @param key
     * @return
     */
    public String getStringValue(String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getString(key, "");
    }

    /**
     * Read an string to SharedPreferences
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getStringValue(String key, String defaultValue) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getString(key, defaultValue);
    }

    /**
     * Save a boolean to SharedPreferences
     *
     * @param key
     * @param
     */
    public void putBooleanValue(String key, Boolean b) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, b);
        editor.commit();
    }

    /**
     * Read a boolean to SharedPreferences
     *
     * @param key
     * @return
     */
    public boolean getBooleanValue(String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getBoolean(key, false);
    }

    /**
     * Save a float to SharedPreferences
     *
     * @param key
     * @param
     */
    public void putFloatValue(String key, float f) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, f);
        editor.commit();
    }

    /**
     * Read an float to SharedPreferences
     *
     * @param key
     * @return
     */
    public float getFloatValue(String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCES,
                0);
        return pref.getFloat(key, 0.0f);
    }
}
