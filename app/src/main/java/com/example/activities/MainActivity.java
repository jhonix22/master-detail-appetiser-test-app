package com.example.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.adapters.ItemsAdapter;
import com.example.masterdetailappetiser.R;
import com.example.models.ItuneItem;
import com.example.models.ItuneResponse;
import com.example.persistence.PreferenceManager;
import com.example.util.ConnectivityHelper;
import com.example.viewmodel.ItunesViewModel;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ItuneItem> ituneItemArrayList = new ArrayList<>();
    private ItemsAdapter itemsAdapter;
    private RecyclerView recyclerView;
    private ItunesViewModel itunesViewModel;
    private ProgressBar progressBar;
    private PreferenceManager preferenceManager;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.item_list_recycleview);
        progressBar = findViewById(R.id.progressBar);

        TextView last_visit_status = findViewById(R.id.last_visit_status);

        setSupportActionBar(toolbar);

        Log.d(TAG, "onCreate: ViewModel Init Provider");

        //instantiate preferencemanager
        preferenceManager = PreferenceManager.getInstance(this);


        //check if user is first time visiting the app via LAST_VISIT data

        if(preferenceManager.getLAST_VISIT().equals("")){
            //pull current date and time
            String date = new SimpleDateFormat("MMMM dd, yyyy h:mm a", Locale.getDefault()).format(new Date());

            //set last visit info
            preferenceManager.setLAST_VISIT("Last visit: " + date);

            //set initial welcome message if LAST_VISIT was not set
            last_visit_status.setText(getResources().getString(R.string.first_time_visit));
        }else{
            //display last visit info
            last_visit_status.setText(preferenceManager.getLAST_VISIT());

            //get current date and time
            String date = new SimpleDateFormat("MMMM dd, yyyy h:mm a", Locale.getDefault()).format(new Date());

            //set last visit
            preferenceManager.setLAST_VISIT("Last visit: " + date);
        }


        if (ConnectivityHelper.isConnectedToNetwork(this)) {
            //Show the connected screen
            // initialize ViewModel
            itunesViewModel = ViewModelProviders.of(this).get(ItunesViewModel.class);
            //call initial search from the view model
            itunesViewModel.searchItems("star", "movie");
            //show progress bar on call
            showProgressBar();

            //set observable
            itunesViewModel.getItunesRepository().observe(this, new Observer<ItuneResponse>() {

                /**
                 * When data is changed, get data and add to our array list
                 * add fetched data to the adapter to be displayed in the recycle view
                 * @param ituneResponse The new data
                 */
                @Override
                public void onChanged(ItuneResponse ituneResponse) {
                    Log.d(TAG, "onChanged: " + new Gson().toJson(ituneResponse));
                    List<ItuneItem> ituneItemList = ituneResponse.getResults();
                    ituneItemArrayList.addAll(ituneItemList); //add return list to our array list
                    itemsAdapter.notifyDataSetChanged(); //notify changes to the adapter
                    hideProgressBar(); //hide progress bar
                }
            });
        } else {
            //Show disconnected screen
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }



        //call initRecycleView method
        initRecycleView();
    }

    /**
     * method to initialize the items adapter and the recycleview layout
     */

    private void initRecycleView(){
        Log.d(TAG, "initRecycleView: init RecycleView");
        if(itemsAdapter  == null){
            itemsAdapter = new ItemsAdapter(MainActivity.this, ituneItemArrayList);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(itemsAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }else{
            itemsAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Show progressbar
     */

    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide progressbar
     */
    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

}
