package com.example.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.models.ItuneResponse;
import com.example.repositories.ItunesRepository;

public class ItunesViewModel extends ViewModel {

    private MutableLiveData<ItuneResponse> mutableLiveData;

    private ItunesRepository itunesRepository;

    /**
     * Initialize data
     */
    public void searchItems(String term, String media){

        if(mutableLiveData != null){
            return;
        }

        //initialize repository instance
        itunesRepository = ItunesRepository.getInstance();
        mutableLiveData = itunesRepository.getItuneResponse(term,"au", media);
    }

    /**
     * This method will return the response from the api as live data
     * @return A response from the repository
     */

    public LiveData<ItuneResponse> getItunesRepository() {
        return mutableLiveData;
    }

}
