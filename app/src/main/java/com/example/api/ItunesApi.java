package com.example.api;

import com.example.models.ItuneResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ItunesApi {
    @GET("/search")
    Call<ItuneResponse> getItems (@Query("term") String term, @Query("country") String country, @Query("media") String media);
}
