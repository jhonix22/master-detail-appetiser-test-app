package com.example.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class will serve as a generator in creating a service
 * to avoid
 */

public class RetrofitService {

    // retrofit instance
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://itunes.apple.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    /**
     * Dynamically create a service
     * @param serviceClass
     * @param <S>
     * @return A service
     */
    public static <S> S createService (Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
 }
