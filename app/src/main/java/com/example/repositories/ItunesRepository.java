package com.example.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.api.ItunesApi;
import com.example.models.ItuneResponse;
import com.example.service.RetrofitService;
import com.google.gson.Gson;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItunesRepository {

    private static ItunesRepository itunesRepository;
    private static final String TAG = "ItunesRepository";

    /**
     * initialize repository
     * @return returns the repository
     */
    public static ItunesRepository getInstance() {
        if(itunesRepository == null){
            itunesRepository = new ItunesRepository();
        }
        return itunesRepository;
    }

    private ItunesApi itunesApi;

    /**
     * create itunes service via retrofit service generator class
     */
    public ItunesRepository(){
        itunesApi = RetrofitService.createService(ItunesApi.class);
    }

    /**
     * A method that triggers the api request to itunes web service
     * @param term
     * @param country
     * @param media=
     * @return A mutable live data of itune response received from the api
     */

    public MutableLiveData<ItuneResponse> getItuneResponse(String term, String country, String media){
        Log.d(TAG, "getItuneResponse: "+ term + " " +country + " " + media);
        final MutableLiveData<ItuneResponse> ituneResponse = new MutableLiveData<>();

        itunesApi.getItems(term, country, media).enqueue(new Callback<ItuneResponse>() {
            @Override
            public void onResponse(Call<ItuneResponse> call, Response<ItuneResponse> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse: " + new Gson().toJson(response.body()));
                    ituneResponse.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ItuneResponse> call, Throwable t) {
                    ituneResponse.setValue(null);
            }
        });
        return ituneResponse;
    }

}
